package helloworld;

public class Inyeccion {
	
	String nombre;
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public void saluda(){
		System.out.println("Saluda" + " " + this.nombre);
	}

}
