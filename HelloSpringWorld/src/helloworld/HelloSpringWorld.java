package helloworld;

public class HelloSpringWorld {
	
	Inyeccion inyeccionBean;
	
	public void greeting(String name){
		System.out.println("Hello world and welcome to Spring " + name);
		inyeccionBean.saluda();
	}
	
	public Inyeccion getInyeccionBean() {
		return inyeccionBean;
	}
	public void setInyeccionBean(Inyeccion inyeccion) {
		this.inyeccionBean = inyeccion;
	}

}
