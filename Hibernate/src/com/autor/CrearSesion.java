
package com.autor;

import java.util.List;

import org.hibernate.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

public class CrearSesion {

	public static void main(String[] args) {
		
		Autor autor = new Autor();
		autor.setNombre("Alex");

		try{
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			
			//Insert
			//session.save(autor);
			
			//Select HQL
			//Query q = session.createQuery("from Autor t");
			//List list = q.list();
			
			////Criteria
			//Criteria criteria = session.createCriteria(Autor.class);
			//criteria.add(null);
			//List list = criteria.list();
			
			//Select SQL
			//SQLQuery q = session.createSQLQuery("SELECT * FROM AUTORES");
			//List <String> list = q.list();
			
			session.flush();
			tx.commit();
			session.close();
		}
		catch(HibernateException ex){
			 System.out.println(ex.getMessage());
			 ex.printStackTrace();
			 System.out.println(ex.toString());
		}
		
	}

}