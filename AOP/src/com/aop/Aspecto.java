package com.aop;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.ThrowsAdvice;

public class Aspecto implements MethodBeforeAdvice, AfterReturningAdvice, ThrowsAdvice, MethodInterceptor {

	@Override
	public void before(Method method, Object[] parametros, Object target)
			throws Throwable {

		System.out.println("MethodBeforeAdvice");
	}

	@Override
	public void afterReturning(Object arg0, Method arg1, Object[] arg2,
			Object arg3) throws Throwable {
		System.out.println("AfterReturningAdvice");
	}
	
	public void afterThrowing(Method method, Object[] parametros, Object target, Exception e)
			throws Throwable {

		System.out.println(e.getMessage());
	}

	@Override
	public Object invoke(MethodInvocation arg0) throws Throwable {
		System.out.println("MethodInterceptor");
		
		return arg0.proceed();
	}

}