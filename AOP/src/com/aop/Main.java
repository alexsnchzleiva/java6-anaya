package com.aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String [] args){

		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		
		Acciones servicio = (Acciones) context.getBean("proxyAcciones");

		servicio.despide();
		
	}
}